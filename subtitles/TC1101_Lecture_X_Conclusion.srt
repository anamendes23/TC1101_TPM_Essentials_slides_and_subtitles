1
00:00:01,520 --> 00:00:07,240
Welcome to the last lecture of this
course. I hope the many exercises were

2
00:00:07,240 --> 00:00:12,400
useful. Some of them were a bit
straightforward others looked easy but

3
00:00:12,400 --> 00:00:17,480
turned out to be difficult. The important
thing is that you now have an insight a

4
00:00:17,480 --> 00:00:23,920
personal insight about what the TPM can
do and what the TPM cannot do. This is

5
00:00:23,920 --> 00:00:28,519
important because of the rising
requirements for security and the actual

6
00:00:28,519 --> 00:00:32,960
needs of protecting our data better.
And this is just the start this is the

7
00:00:32,960 --> 00:00:37,680
essentials course. So it's natural to
have a learning curve. The important

8
00:00:37,680 --> 00:00:42,899
thing is that you took the first step
and good job on completing this course.

9
00:00:42,899 --> 00:00:48,920
Why did we do this today? Why did
we have this course now? Why not tomorrow?

10
00:00:48,920 --> 00:00:54,079
There's plenty of time right? Well to
answer this question I need to take you

11
00:00:54,079 --> 00:00:59,719
just a month back and tell you a little
story. Last month I was invited to lead a

12
00:00:59,719 --> 00:01:04,479
discussion panel about the future
of IoT security. And as the moderator I

13
00:01:04,479 --> 00:01:09,360
was expected to provide the topics. One
of the topics was post-quantum

14
00:01:09,360 --> 00:01:14,320
cryptography. I'm very interested in this
topic but to be honest I have a

15
00:01:14,320 --> 00:01:18,600
fundamental knowledge and understanding
of what's happening right now in this

16
00:01:18,600 --> 00:01:24,680
space, but I would not call myself an
expert. The question, the only question I

17
00:01:24,680 --> 00:01:29,000
asked about the topic, and the
conversation just took off, was where we

18
00:01:29,000 --> 00:01:32,479
are in the Hype Cycle.
And this is the Hype Cycle as defined by

19
00:01:32,479 --> 00:01:38,360
Gartner, famous consultants. And while
there are other variations of this

20
00:01:38,360 --> 00:01:43,360
diagram this one seems to be the most
popular and it's a fairly good

21
00:01:43,360 --> 00:01:50,322
approximation of a lot of trends that
happen in technology recently.

22
00:01:50,322 --> 00:01:55,240
So why did we do this course
today why not tomorrow? Let's try to

23
00:01:55,240 --> 00:02:01,119
answer that question. My journey started
in 2014 when I was asked to develop a

24
00:02:01,119 --> 00:02:07,039
remote attestation solution for a router
manufacturer. A professional equipment

25
00:02:07,039 --> 00:02:12,160
for routing the telecommunications of a
whole city. These were rooms filled with

26
00:02:12,160 --> 00:02:19,720
racks, each rack was an x86 server with
massive Xeon CPU and so on and so on.

27
00:02:19,720 --> 00:02:27,560
And it had a TPM 1.2 this was 2014 TPM 2.0
was not mass available. In fact it was

28
00:02:27,560 --> 00:02:32,560
very difficult to purchase at the time
because the production was just

29
00:02:32,560 --> 00:02:38,920
scaling. So this is how my journey
started and it took me a while to figure

30
00:02:38,920 --> 00:02:46,480
out how things happen in the industry.
Tor me the biggest shock was that this

31
00:02:46,480 --> 00:02:55,440
very useful technology at the time was
used so little and yet in so many places.

32
00:02:55,440 --> 00:03:00,400
It's a paradox.
On the one side it was already

33
00:03:00,400 --> 00:03:06,480
in almost every x86 personal computer. On
the other hand it was very difficult to

34
00:03:06,480 --> 00:03:12,080
use. It there was the TrouSerS
TPM 1.2 software stack.

35
00:03:12,080 --> 00:03:16,480
It had a lot of problems, I myself had to
fix a bug to use it for remote attestation

36
00:03:16,480 --> 00:03:21,920
that I remember. And just going through
this experience I can definitely tell

37
00:03:21,920 --> 00:03:26,920
you there was a lot of excitement in the
year afterwards, and maybe the up to

38
00:03:26,920 --> 00:03:31,480
around 2017.
When the TPM 2.0 was now mass

39
00:03:31,480 --> 00:03:36,720
available. It started replacing TPM 1.2
in personal computers, in servers, there

40
00:03:36,720 --> 00:03:43,720
was big talks about entering, I don't
remember, but there there was a deal with

41
00:03:43,720 --> 00:03:48,640
a cinematic company even announced at
one point that was going to use TPM in

42
00:03:48,640 --> 00:03:54,720
some way. And this sparked a lot of
controversy right oh protecting content

43
00:03:54,720 --> 00:04:00,640
and whatnot yeah. But at the same time
I've experienced the usefulness of this

44
00:04:00,640 --> 00:04:05,640
chip in protecting the counter of the
network adapter of this professional

45
00:04:05,640 --> 00:04:10,239
router. And actually just a number of
professional routers that were working

46
00:04:10,239 --> 00:04:15,239
together. And how this solved an actual
business problem. On the other hand then

47
00:04:15,239 --> 00:04:19,799
we used it to further improve the
security of the router itself. So I was

48
00:04:19,799 --> 00:04:25,080
very excited and you can probably sense
it in my voice about this technology. 

49
00:04:25,080 --> 00:04:31,560
So it was to my shock to understand that in
2015 there was only SAPI available, the

50
00:04:31,560 --> 00:04:37,680
system API as defined by the
TCG. It was I think about two years later

51
00:04:37,680 --> 00:04:43,440
at embedded world when I learned that
finally ESAPI was implemented.

52
00:04:43,440 --> 00:04:48,039
I remember it was, the work was sponsored
by Infineon if I'm not

53
00:04:48,039 --> 00:04:53,440
mistaken, and it was a big news it was a
big moment for the whole industry that

54
00:04:53,440 --> 00:04:59,400
now we have this more Rich API to use
and implement applications. At the same

55
00:04:59,400 --> 00:05:04,479
time time I remember that this
transition kind of broke what I've built

56
00:05:04,479 --> 00:05:10,160
so far because I've relied so much on SAPI
and the tools kind of transitioned to

57
00:05:10,160 --> 00:05:16,759
ESAPI. So why am you telling you this
story well this was 2015, 2017 this is

58
00:05:16,759 --> 00:05:20,919
when things were really really excited
and there were a lot of expectations

59
00:05:20,919 --> 00:05:25,639
about what would come next. Of course
there is a knowledge barrier there's a

60
00:05:25,639 --> 00:05:31,319
know-how barrier, there's a learning curve.
And all of this played part. But things

61
00:05:31,319 --> 00:05:39,479
changed. Today it's 2023 and we have so
much more. Before talking about today's

62
00:05:39,479 --> 00:05:45,600
state I want to talk a bit more about
what actually took place in these years

63
00:05:45,600 --> 00:05:51,360
to help you understand why it is
important to take this course today, and

64
00:05:51,360 --> 00:05:55,800
why it is great for you that you
completed this course and gained this knowledge.

65
00:05:55,800 --> 00:06:00,039
The story of the TCG starts in
1999

66
00:06:00,039 --> 00:06:05,120
back then it was called with a different
name. I think Compaq was also part of the

67
00:06:05,120 --> 00:06:10,160
consortium that started the TPM
specification. It was only two years

68
00:06:10,160 --> 00:06:16,479
later when the first TPM version actual
hardware chip was manufactured by IBM.

69
00:06:16,479 --> 00:06:22,880
And at the time IBM I think was the sole
manufacturer of this chip. So it's no

70
00:06:22,880 --> 00:06:28,479
surprise that in 2002 the first
commercial product off the shelf that

71
00:06:28,479 --> 00:06:33,440
anyone could buy
was ThinkPad T30 that featured the TPM 1.0

72
00:06:33,440 --> 00:06:38,080
And remember this is at the very
beginning this is what Gartner call a

73
00:06:38,080 --> 00:06:42,520
technology trigger. For me it's important
that this was the moment when people

74
00:06:42,520 --> 00:06:47,039
start to experience okay what is this
technology about what can we do with it.

75
00:06:47,039 --> 00:06:51,919
It took a long time before the
technology itself became standardized.

76
00:06:51,919 --> 00:06:56,280
And this is a difficult task it takes a
lot of time to go through the motions of

77
00:06:56,280 --> 00:07:00,960
creating a standard and also have it in
the proper form as text, and and comply

78
00:07:00,960 --> 00:07:06,240
with the rules there are to actually put
a standard forward that is a worldwide

79
00:07:06,240 --> 00:07:12,520
recognized ISO/EIC standard. It
contains four parts which match the TCG

80
00:07:12,520 --> 00:07:17,319
specification. We have structures
command architecture and so on. So if

81
00:07:17,319 --> 00:07:22,800
you read the standard it very similar to
the specification. And it gets occasional

82
00:07:22,800 --> 00:07:28,479
updates because as the TCG continues to
work on improving the TPM the standard

83
00:07:28,479 --> 00:07:34,240
needs adopt these changes as well.
So what was the pivotal moment? Again I

84
00:07:34,240 --> 00:07:39,759
think it was around 2014-15 when
everyone was already trying to use TPM

85
00:07:39,759 --> 00:07:45,039
1.2. And we were seeing the value but at
the same time it was difficult some

86
00:07:45,039 --> 00:07:50,000
things were not working there were
issues. So when TPM 2.0 became available,

87
00:07:50,000 --> 00:07:56,039
naturally companies and professionals
became very excited. Afterwards we got

88
00:07:56,039 --> 00:08:00,120
the ESAPI implemented. In 2019 came the
FAPI.

89
00:08:00,120 --> 00:08:07,080
Something else happened at that time as
well. The TPM cost came down. And many new

90
00:08:07,080 --> 00:08:11,560
TPM stacks came to life.
This is very important for the

91
00:08:11,560 --> 00:08:17,080
adoption of one technology: to become
accessible and affordable. On one hand we

92
00:08:17,080 --> 00:08:23,080
have many TPM vendors. On the other hand
we have many new TPM stacks, and at the

93
00:08:23,080 --> 00:08:30,000
same time we now can afford purchasing a
TPM chip for $1 per unit

94
00:08:30,000 --> 00:08:35,360
when we order a certain quantity. And
this is the major difference between

95
00:08:35,360 --> 00:08:39,760
2015 and 2019. This is a major shift for the

96
00:08:39,760 --> 00:08:47,600
industry. In 2015 we had to order 100,000
pieces to get the $1 per unit price.

97
00:08:47,600 --> 00:08:53,839
In 2019 this changed. We could order only
10,000 pieces and we would still get the

98
00:08:53,839 --> 00:08:59,519
$1 per unit price. This happened for
various reason: the market was was more

99
00:08:59,519 --> 00:09:04,320
mature, there was more capacity at the
manufacturing plants, and the TPM was

100
00:09:04,320 --> 00:09:09,880
more widely adopted.
Now there's was a bit of a going

101
00:09:09,880 --> 00:09:15,800
back around 2021 because of the chip
shortage that hit everyone worldwide.

102
00:09:15,800 --> 00:09:21,600
But I feel nowadays in 2023 things are back
to normal. And why this is important?

103
00:09:21,600 --> 00:09:26,839
So, starting from startups, who manufacture
maybe in the realm of thousands at the

104
00:09:26,839 --> 00:09:31,360
beginning, going through small and medium
businesses who manufacture devices in

105
00:09:31,360 --> 00:09:37,200
maybe the 10,000s to 100,000 devices,
having this changed allows more

106
00:09:37,200 --> 00:09:42,079
companies more products to integrate the
TPM. And this is what we're seeing today.

107
00:09:42,079 --> 00:09:48,160
More embedded systems are integrating
the TPM; aerospace, medical, you name it.

108
00:09:48,160 --> 00:09:54,040
So this is a big change. The accessibility
of the device itself is important.

109
00:09:54,040 --> 00:09:59,920
But what's also important is what software we
can use what tools are available to

110
00:09:59,920 --> 00:10:05,120
actually integrate that chip into our
system. Or use that chip if it's already

111
00:10:05,120 --> 00:10:09,440
part of the system especially when we
talk about servers and laptops. And there

112
00:10:09,440 --> 00:10:14,279
are many many good things that happen to
the TPM software stacks throughout the

113
00:10:14,279 --> 00:10:20,680
years. especially after the first
introduction of SAPI in 2015. By 2018 we

114
00:10:20,680 --> 00:10:26,600
had five different TPM software
libraries that we can choose from.

115
00:10:26,600 --> 00:10:33,880
We added the Microsoft stack with the major
version 2 for its .NET version and

116
00:10:33,880 --> 00:10:39,000
almost every possible TPM functionality
supported. This is a major milestone for

117
00:10:39,000 --> 00:10:47,760
Microsoft and the industry. Then by 2018
we also had the wolfTPM stack emerging

118
00:10:47,760 --> 00:10:53,160
and the Google TPM stack emerging. Go-TPM
today is a project with many

119
00:10:53,160 --> 00:10:59,760
contributors as well. So it's no longer
the Google stack it is the Go-lang stack.

120
00:10:59,760 --> 00:11:07,880
At the same time I see a lot of effort
going into the new stacks as well as the

121
00:11:07,880 --> 00:11:14,240
old and very mature solutions. For
example at the beginning go-tpm didn't

122
00:11:14,240 --> 00:11:19,920
have one to one mapping with TPM commands.
wolfTPM had it from the beginning, and

123
00:11:19,920 --> 00:11:25,320
later added wrappers. While go-tpm was
going toward a mild layer on top from

124
00:11:25,320 --> 00:11:29,079
the very beginning. So we saw two very
different design patterns at the

125
00:11:29,079 --> 00:11:37,440
beginning and later they kind of met. Now
both stacks have one to one mapping and

126
00:11:37,440 --> 00:11:42,959
wrappers. For rich operations rich API
similar to the FAPI and ESAPI that

127
00:11:42,959 --> 00:11:49,079
we mention often in this
course. So this is important all of these

128
00:11:49,079 --> 00:11:54,079
stacks today are quite mature. They
support almost every possible TPM

129
00:11:54,079 --> 00:11:58,240
operation there is. There's still some
corner cases maybe here and there.

130
00:11:58,240 --> 00:12:02,600
But they're open-source libraries. You can
raise an issue in GitHub and you'll get

131
00:12:02,600 --> 00:12:07,760
responses you get help which is
important. Also now we have TPM

132
00:12:07,760 --> 00:12:13,320
communities we have also the OST2 TPM
course which has discussion boxes where

133
00:12:13,320 --> 00:12:19,199
we can also discuss problems and
challenges. So I feel very optimistic for

134
00:12:19,199 --> 00:12:24,839
what's coming next. And in my opinion
this is the plateau of productivity, if

135
00:12:24,839 --> 00:12:29,079
we look back at the Hype Cycle. Meaning
that we are out of the Hype Cycle and

136
00:12:29,079 --> 00:12:35,360
entering the mass adoption of the TPM by
small medium and large organizations in

137
00:12:35,360 --> 00:12:41,199
the next maybe 10 years or so. So you
being here at the beginning of this is

138
00:12:41,199 --> 00:12:46,160
important. You have a competitive
advantage at the job market. And you can

139
00:12:46,160 --> 00:12:52,680
help your company your product be more
secure. What can you do more? Well we

140
00:12:52,680 --> 00:12:58,800
first ask ourselves what can we do more
for you and this is we're working on an

141
00:12:58,800 --> 00:13:05,839
an advanced TPM course. In the meantime I
have some ideas for you first please

142
00:13:05,839 --> 00:13:09,839
look at your daily workflow look at the
product or the project that you're

143
00:13:09,839 --> 00:13:15,440
currently working. Isn't there a way to
integrate the TPM to help your security?

144
00:13:15,440 --> 00:13:20,680
To be more sure of how the device
operates or protect the sensitive data

145
00:13:20,680 --> 00:13:25,199
the sensitive parameters and
configuration files? I believe even if

146
00:13:25,199 --> 00:13:29,920
you look at your personal computer you
can use the TPM to protect your personal

147
00:13:29,920 --> 00:13:35,279
folder, or folder with notes, or with
drawings. And I think this is a great

148
00:13:35,279 --> 00:13:41,160
experiment. Experiment more. Create your
own TPM application in your free time,

149
00:13:41,160 --> 00:13:45,480
and see how useful that is. Make a
prototype for the project that you're

150
00:13:45,480 --> 00:13:50,519
currently on. Propose it in a product
meeting and see the responses have a

151
00:13:50,519 --> 00:13:55,920
brainstorming session. This is sort of a
challenge after the course is

152
00:13:55,920 --> 00:14:01,880
over. Then if you're looking for more
interesting technology coming out of the

153
00:14:01,880 --> 00:14:06,040
Trusted Computing Group and in general
what's coming next in the Trusted

154
00:14:06,040 --> 00:14:12,422
Computing domain, there is a framework
originally started by Microsoft, called DICE.

155
00:14:12,422 --> 00:14:18,959
I call it a framework for computer
trust because it can work with different

156
00:14:18,959 --> 00:14:24,079
hardware security modules. And even
without in some cases. Where you have an

157
00:14:24,079 --> 00:14:31,759
SoC, a very embedded device, and it has NVRAM
that can be burned once written once

158
00:14:31,759 --> 00:14:37,440
and then you can have a secure boot
process. You can anchor the root of trust.

159
00:14:37,440 --> 00:14:41,959
And from there you can make some
guarantees. So DICE provides some

160
00:14:41,959 --> 00:14:46,959
flexibility is an interesting thing.
And maybe it is not at the same stage as the

161
00:14:46,959 --> 00:14:53,279
TPM as maturity, maybe it's not there yet
at the plateau of productivity, but I

162
00:14:53,279 --> 00:14:57,959
think it's definitely a Trusted
Computing technology that we all should

163
00:14:57,959 --> 00:15:04,839
look at and help develop further. Now
comes time to my personal favorite a

164
00:15:04,839 --> 00:15:09,399
very new Trusted Computing technology
called MARS in my opinion MARS is a

165
00:15:09,399 --> 00:15:14,040
lightweight TPM. It is something that the
industry has been asking for a long long

166
00:15:14,040 --> 00:15:23,079
time. MARS uses less algorithms less
space less memory in general. It is

167
00:15:23,079 --> 00:15:29,079
designed to be integrated in silicon
solutions so in chips. But in theory it

168
00:15:29,079 --> 00:15:33,839
can be put on an FPGA and this is how it's
being currently tested and you can add

169
00:15:33,839 --> 00:15:40,160
it to your product even today. Now how
production ready that is I couldn't tell.

170
00:15:40,160 --> 00:15:44,440
For me it would be maybe a bit too early.
This would be for early adopters.

171
00:15:44,440 --> 00:15:50,240
For companies and people professionals who
really need such lightweight solution.

172
00:15:50,240 --> 00:15:54,839
But with time I believe that MARS will
be a valuable contribution to the

173
00:15:54,839 --> 00:16:00,160
Trusted Computing available tools.
And feel free to just just check out the TCG

174
00:16:00,160 --> 00:16:06,720
site, the TPM communities, and learn more
about the available solutions and new

175
00:16:06,720 --> 00:16:11,399
projects. I can assure you there are many.
Including there's now a TPM on a USB

176
00:16:11,399 --> 00:16:16,639
stick. If you're a Mac user there is a
way for you to to use the TPM an actual

177
00:16:16,639 --> 00:16:21,519
physical hardware TPM and protect your
let's say personal folder or create your

178
00:16:21,519 --> 00:16:27,319
own TPM application. As mentioned at the
beginning of this slide experiment more.

179
00:16:27,319 --> 00:16:32,319
I want to thank everyone that took the
course and this course would not have

180
00:16:32,319 --> 00:16:38,360
been possible without the belief of
Xeno Kovah the founder of OpenSecurityTraining2

181
00:16:38,360 --> 00:16:44,920
The Trusted Computing field is important and it needs more

182
00:16:44,920 --> 00:16:50,240
resources for learning. It needs more
practical resources for people who want

183
00:16:50,240 --> 00:16:55,959
to gain more skills into the field of
computer security. We worked for a long

184
00:16:55,959 --> 00:17:03,319
time with Xeno to bring this course to
life. And at times it was uncertain but

185
00:17:03,319 --> 00:17:08,880
we kept at it. So I want to thank him for
staying the course and showing

186
00:17:08,880 --> 00:17:14,000
persistence in this
effort. Now I want also to thank our

187
00:17:14,000 --> 00:17:21,839
video editor who has done a great job
following our requests of how to do the

188
00:17:21,839 --> 00:17:27,640
videos what to change how to help us
make them better. And we hope that you

189
00:17:27,640 --> 00:17:32,919
like the quality that we provide you. We
really tried to go for the highest

190
00:17:32,919 --> 00:17:38,760
quality possible at the time. And you
would notice that our videos are in 4K

191
00:17:38,760 --> 00:17:45,640
format. This is just one example of the
work. Then I really want to thank the

192
00:17:45,640 --> 00:17:49,240
people who took this course because the
beta

193
00:17:49,240 --> 00:17:55,520
testers would help shape what we've
built. And this is important because we

194
00:17:55,520 --> 00:18:02,679
are also human we make mistakes we miss
things. And it's a learning curve for us

195
00:18:02,679 --> 00:18:07,880
what's really needed for the students.
Where we need to put more effort to

196
00:18:07,880 --> 00:18:12,320
explain better to explain more maybe
provide more examples and so on and so on.

197
00:18:12,320 --> 00:18:17,440
Then for the professionals and
students who are seeing this technology

198
00:18:17,440 --> 00:18:21,640
for the first time we hope that you
found a valuable

199
00:18:21,640 --> 00:18:27,640
resource. We hope that this serves you as
a guide and as a tool for the future so

200
00:18:27,640 --> 00:18:32,039
you can always come back and look at the
course and remember something repeat

201
00:18:32,039 --> 00:18:37,720
something and find a solution for a
problem. Last but not least I want to

202
00:18:37,720 --> 00:18:42,360
thank the Trusted Computing Group and
all of its members for the work that

203
00:18:42,360 --> 00:18:47,960
they're doing. Because without the
specifications that make the TPM a

204
00:18:47,960 --> 00:18:53,559
standard tool, standard way of
communicating with the TPM, standard set

205
00:18:53,559 --> 00:18:59,559
of TPM commands, and
properties. Yes standardization is not

206
00:18:59,559 --> 00:19:05,159
perfect but it helps us have this tool
that we can put in embedded systems, in

207
00:19:05,159 --> 00:19:10,159
servers, in laptops, even in phones if
you think about it. At one point there

208
00:19:10,159 --> 00:19:15,799
was even an automotive TPM. There is
still a spec about it I think. My point

209
00:19:15,799 --> 00:19:22,520
is that TCG does a hard work. And it's
not always a public work. It becomes

210
00:19:22,520 --> 00:19:26,520
public after time when the drafts have
been made and a lot of work has already

211
00:19:26,520 --> 00:19:31,799
gone into the documents and into the
process. This initial work on

212
00:19:31,799 --> 00:19:37,640
standardization of the TPM
separates it as a solution when we

213
00:19:37,640 --> 00:19:42,760
compare it with other hardware security
modules and secure elements. So thank you.

214
00:19:42,760 --> 00:19:48,919
Send us your questions at our course
email or write us in the discussion box

215
00:19:48,919 --> 00:19:53,760
after each unit till we meet again.

