1
00:00:00,760 --> 00:00:07,639
Welcome to this OST2 lecture about TPM
keys, key generation, key management, and

2
00:00:07,639 --> 00:00:13,839
hierarchies. Many TPM operations cannot
work without cryptographic keys. The

3
00:00:13,839 --> 00:00:20,720
generation and use of cryptographic keys
by the TPM is vital for its operations.

4
00:00:20,720 --> 00:00:26,560
Therefore we will take a bit of time to
understand how it internally works.

5
00:00:26,560 --> 00:00:32,000
First and foremost, we need to know that
private key material can never leave the

6
00:00:32,000 --> 00:00:38,440
TPM in plain form. The keys are either
encrypted or not stored. Keys that are

7
00:00:38,440 --> 00:00:43,960
usually not stored are primary keys that
can be regenerated every time. 

8
00:00:43,960 --> 00:00:48,600
To regenerate these keys, of course, we need
to have the proper authorization and

9
00:00:48,600 --> 00:00:53,760
also we need to know what seed was used.
There is an internal seed built in the

10
00:00:53,760 --> 00:01:00,000
TPM put there by the manufacturer. And
also the TPM allows the user or the TPM

11
00:01:00,000 --> 00:01:05,159
owner to provide additional seed
additional entropy to the key derivation

12
00:01:05,159 --> 00:01:13,320
function KDF for short. By using the same
user entropy added to the internal seed,

13
00:01:13,320 --> 00:01:17,799
we can guarantee that we will generate
the same primary key. The private

14
00:01:17,799 --> 00:01:24,479
material of private keys never leaves
the TPM. We can only regenerate them.

15
00:01:24,479 --> 00:01:30,640
There are some optimizations we can do
to avoid the regeneration, like persisting the TPM

16
00:01:30,640 --> 00:01:36,799
key. And this is covered in one of our
other lectures. Child keys are typically

17
00:01:36,799 --> 00:01:41,560
the keys that are used to perform the
various cryptographic operations, like

18
00:01:41,560 --> 00:01:48,119
signing, or encryption, and so on. Where
primary keys are used to encrypt and

19
00:01:48,119 --> 00:01:53,600
protect the child keys. We also call this
wrapping. The primary key is wrapping the

20
00:01:53,600 --> 00:01:58,360
child key. This is also one of the
reasons why in order to create a child

21
00:01:58,360 --> 00:02:02,799
key we first need to have a primary key
to load the child key we also need to

22
00:02:02,799 --> 00:02:07,880
have the authorization for the primary
key in order to be able to decrypt

23
00:02:07,880 --> 00:02:14,879
unwrap the child key inside the TPM. And
then finish the process of loading so we

24
00:02:14,879 --> 00:02:20,920
can actually use it to perform some
operations. A fundamental feature of the

25
00:02:20,920 --> 00:02:26,599
TPM is that it can have many key
hierarchies. The TPM has fixed

26
00:02:26,599 --> 00:02:32,680
hierarchies that always exist, and key
hierarchies can be created by the owner.

27
00:02:32,680 --> 00:02:38,599
They are defined by the primary key.
Under the owner hierarchy, we have as

28
00:02:38,599 --> 00:02:43,120
many as we want key
hierarchies. This is also true for the

29
00:02:43,120 --> 00:02:48,239
other hierarchies, but it is unlikely
that we'll do so. Because of the special

30
00:02:48,239 --> 00:02:54,159
purpose of different hierarchies usually
the owner hierarchy is mostly used.

31
00:02:54,159 --> 00:02:59,840
For example, the platform hierarchy is where
the OEM the system builder or integrator

32
00:02:59,840 --> 00:03:06,920
places different objects. The OEM
initializes these objects or puts them

33
00:03:06,920 --> 00:03:10,480
there during manufacturing.
The platform

34
00:03:10,480 --> 00:03:16,239
authorization is randomized at system boot
usually. And this is done by the first

35
00:03:16,239 --> 00:03:21,040
OEM software, would that be a UEFI, would
that be BIOS of some kind, or other

36
00:03:21,040 --> 00:03:26,560
firmware that's starts first. The
platform hierarchy starts with an empty

37
00:03:26,560 --> 00:03:29,439
authorization that is randomized on
every start, that's what we need to

38
00:03:29,439 --> 00:03:32,560
remember here. On the other hand, the endorsement

39
00:03:32,560 --> 00:03:37,200
hierarchy is what's used by the TPM
vendor to place some special objects for

40
00:03:37,200 --> 00:03:42,959
example the endorsement key. The
endorsement key is a unique primary key

41
00:03:42,959 --> 00:03:48,959
that is created using the internal seed
of the TPM and has properties defined by

42
00:03:48,959 --> 00:03:53,519
the Trusted Computing Group. Of course
for your purposes you can create

43
00:03:53,519 --> 00:03:59,680
different primary keys under the
endorsement hierarchy. It's all the

44
00:03:59,680 --> 00:04:03,159
depending on your
requirements. The good thing about the

45
00:04:03,159 --> 00:04:08,439
endorsement key is that we can use it
for machine identity in a standardized way. 

46
00:04:08,439 --> 00:04:13,319
The owner hierarchy we already covered, you
can have as many keys as you want, and

47
00:04:13,319 --> 00:04:16,560
the good thing is that each primary key
has its own authorization.

48
00:04:16,560 --> 00:04:21,239
So the higher key
authorization does not prevent you to

49
00:04:21,239 --> 00:04:26,080
have different key owners below. Which is
important especially for large systems,

50
00:04:26,080 --> 00:04:32,680
automotive, and others. The last hierarchy
is more special than the others in that

51
00:04:32,680 --> 00:04:42,759
it seed is never the same. The so-called
NULL hierarchy is seeded at every power

52
00:04:42,759 --> 00:04:49,720
cycle of the TPM. This means actual power
off and power on. If you just put your

53
00:04:49,720 --> 00:04:53,160
computer to sleep there's no guarantee
that the seed will change, but if you

54
00:04:53,160 --> 00:04:57,320
turn it off completely and then start it
again, then the TPM guarantees that the

55
00:04:57,320 --> 00:05:01,600
moment it it has voltage it has power it will be seeded

56
00:05:01,600 --> 00:05:06,400
differently. This is used to generate
temporary keys keys that are used one

57
00:05:06,400 --> 00:05:11,120
time only. The benefit of having
different hierarchies and different

58
00:05:11,120 --> 00:05:14,960
authorizations is that this gives us
flexibility when we are managing the

59
00:05:14,960 --> 00:05:20,319
system. For example, we can have a device
that is managed by an organization control

60
00:05:20,319 --> 00:05:25,120
the endorsement
hierarchy. On the other hand, the end user

61
00:05:25,120 --> 00:05:31,039
can control the owner hierarchy. This is
very often the case in manage scenarios.

62
00:05:31,039 --> 00:05:36,479
All the hierarchies are available for
taking ownership. Perhaps the platform

63
00:05:36,479 --> 00:05:41,840
hierarchy is the most difficult because
if you have a very integrated system the

64
00:05:41,840 --> 00:05:47,319
UEFI or the boot loader already
randomized the platform hierarchy. And

65
00:05:47,319 --> 00:05:52,000
you might not know what that
randomization is what password is set

66
00:05:52,000 --> 00:05:59,080
maybe even fixed. All the rest are
usually empty authorizations that need

67
00:05:59,080 --> 00:06:04,600
to be provision need to be set upon use.
The endorsement hierarchy comes with a

68
00:06:04,600 --> 00:06:11,240
default policy. And this is important to
note. This is defined by the TCG and it's

69
00:06:11,240 --> 00:06:18,280
publicly available we'll talk about more
that later. Primary and child keys are

70
00:06:18,280 --> 00:06:24,560
the two types of keys and we need to
differentiate between the two. Primary

71
00:06:24,560 --> 00:06:31,880
keys can perform operations like signing,
encryption, decryption. But usually they're

72
00:06:31,880 --> 00:06:37,000
used just as a step to define a key
hierarchy, and they're used only to wrap

73
00:06:37,000 --> 00:06:41,280
the child key. Child keys are the one
that we the ones that we usually

74
00:06:41,280 --> 00:06:45,479
associate with different operations.
In each of these steps when

75
00:06:45,479 --> 00:06:51,000
we generate a key there's an internal
seed being used and there is a template

76
00:06:51,000 --> 00:06:56,319
that we can provide in the process.
Either through an API or through using

77
00:06:56,319 --> 00:07:02,240
the tools. This is much more than just
the type of key; is that an RSA key, ECC

78
00:07:02,240 --> 00:07:07,919
key, and so on. Primary keys are always
asymmetric as we mentioned at the

79
00:07:07,919 --> 00:07:14,520
beginning ECC or RSA.
Child keys can be symmetric or key

80
00:07:14,520 --> 00:07:21,720
hash as well. For example, AES128.
Such kind of properties go into the

81
00:07:21,720 --> 00:07:27,800
template that we pass to the KDF
function. Including in that template is

82
00:07:27,800 --> 00:07:33,639
the unique field that allows us to add
user entropy. It is not necessary to use

83
00:07:33,639 --> 00:07:38,240
that field, you just need to know what
was the value of that field. If you're

84
00:07:38,240 --> 00:07:42,879
not using it then it's zeros, that's all
fine, it is important for the generation

85
00:07:42,879 --> 00:07:48,879
of the primary key, or its regeneration.
After that we take seed

86
00:07:48,879 --> 00:07:54,159
from the primary key and a new template
for the child key, again user provided,

87
00:07:54,159 --> 00:07:59,879
and we generate a new key. The second
template again comes completely from us.

88
00:07:59,879 --> 00:08:06,360
So the unique field allows for user entropy,
the key parameters, is it 128-bit or

89
00:08:06,360 --> 00:08:12,720
192-bit AES static uh symmetric key for
example. This is all up to the user to

90
00:08:12,720 --> 00:08:18,080
the TPM owner. We already mentioned some
of the supported algorithms by the TPM.

91
00:08:18,080 --> 00:08:24,520
This varies between different models and
vendors. But there is a minimum that the

92
00:08:24,520 --> 00:08:29,479
TPM must provide that is defined by the
Trusting Computing Group. Here I have just

93
00:08:29,479 --> 00:08:35,440
given an outline of what typically is
supported by a TPM. We have a lecture

94
00:08:35,440 --> 00:08:40,560
where we'll see how we can check the
capabilities of the TPM. Of course the

95
00:08:40,560 --> 00:08:45,160
easiest would be to consult the data
sheet. But to be certain it's best to use

96
00:08:45,160 --> 00:08:49,600
uh some of the internal command the TPM
provide to check what is actually

97
00:08:49,600 --> 00:08:55,640
supported. Aside the usual asymmetric and
symmetric keys that we can find here

98
00:08:55,640 --> 00:09:01,880
there is also this separate category of
keyed hash keys that are used usually for

99
00:09:01,880 --> 00:09:06,320
HMAC operations.
On the left side you have the

100
00:09:06,320 --> 00:09:10,760
abbreviations as we're used to seeing
them. On the right side you have an

101
00:09:10,760 --> 00:09:16,720
example, I have how the tpm2-tools expect
these abbreviations and parameters

102
00:09:16,720 --> 00:09:23,079
passed. Please notice that the key type
and the key size are put together as one

103
00:09:23,079 --> 00:09:27,680
word. And of course there is an
alternative naming scheme where we have

104
00:09:27,680 --> 00:09:33,440
extended names using the NIST
definitions. There are many internal

105
00:09:33,440 --> 00:09:39,600
types when generating a TPM key. It is
easier to use tpm2-tools to generate the

106
00:09:39,600 --> 00:09:44,399
key that we need. And it will come with the
right key properties.

107
00:09:44,399 --> 00:09:50,560
For example, if we need to
generate a signature we'll have 

108
00:09:50,560 --> 00:09:57,160
TPMA_OBJECT_SIGN as a property being
set of that key. If the key will be used

109
00:09:57,160 --> 00:10:02,000
for decryption then it will be
(TPMA_OBJECT)_DECRYPT. Now here comes an

110
00:10:02,000 --> 00:10:07,120
interesting part usually primary keys do
not support signing. And this is by

111
00:10:07,120 --> 00:10:12,120
design. It is possible but by default the
primary key is meant only to wrap the

112
00:10:12,120 --> 00:10:16,680
child key. This is especially true when
we have the endorsement key.

113
00:10:16,680 --> 00:10:21,920
The endorsement key does not have the
capability to generate signatures.

114
00:10:21,920 --> 00:10:27,440
Because it is missing the TPMA_OBJECT_SIGN property. The TPM simply cannot

115
00:10:27,440 --> 00:10:33,640
generate such key. And it is not mandated
by the TCG that such key is a valid

116
00:10:33,640 --> 00:10:37,920
endorsement key. There are other
interesting properties like _FIXEDTPM

117
00:10:37,920 --> 00:10:43,320
and _FIXEDPARENT that restrict a key. And
it cannot be transferred between

118
00:10:43,320 --> 00:10:49,360
different TPMs. Which is in most cases
the default. Once you generate a key it

119
00:10:49,360 --> 00:10:55,959
is fixed to the TPM seed. And only this
TPM can unwrap it load it. Then the

120
00:10:55,959 --> 00:11:00,360
primary key unwrap loads the child key
and so forth and so forth. If you're

121
00:11:00,360 --> 00:11:06,079
interested check the TCG specification
for the TPM library. I've given the exact

122
00:11:06,079 --> 00:11:13,959
table where this information is. To
create a primary key we have a tpm2-tool

123
00:11:13,959 --> 00:11:19,519
with a familiar name. Please note that it
is a one word (tpm2_)createprimary.

124
00:11:19,519 --> 00:11:24,639
As mentioned earlier in most situation we
will be using the owner

125
00:11:24,639 --> 00:11:30,560
hierarchy. As the system owners or as the
TPM user this is the place where we will

126
00:11:30,560 --> 00:11:37,399
create our TPM objects, key hierarchies,
and so on. Of course the tool allows us

127
00:11:37,399 --> 00:11:42,240
to choose a different hierarchy if we need.
There are situations in which we would

128
00:11:42,240 --> 00:11:47,839
prefer to use the endorsement
hierarchy. In some cases, "attestations", a

129
00:11:47,839 --> 00:11:54,079
process to generate evidence about the
system with the help of the TPM, are

130
00:11:54,079 --> 00:11:59,040
created under the endorsement hierarchy. When
selecting a hierarchy make sure to

131
00:11:59,040 --> 00:12:05,040
provide the proper authorization. Usually
the authorization is empty. So when we

132
00:12:05,040 --> 00:12:08,360
start using the owner hierarchy we can
just skip that parameter.

133
00:12:08,360 --> 00:12:14,639
It is recommended to provide a
password for your key. So no one else can

134
00:12:14,639 --> 00:12:20,959
use it. Including no one else could load
it to the TPM. Without password anyone

135
00:12:20,959 --> 00:12:26,480
could use the TPM key you just created.
When selecting a key algorithm remember

136
00:12:26,480 --> 00:12:31,639
that we need to also specify not just
the type of key, RSA or ECC, but also some

137
00:12:31,639 --> 00:12:37,880
properties, either the size of the key or
the curve. Of course there are defaults

138
00:12:37,880 --> 00:12:42,720
that we can use. But it's always better
to define in a more verbose way. So you

139
00:12:42,720 --> 00:12:46,959
know exactly the type of key you have
that it's compatible with the system and

140
00:12:46,959 --> 00:12:53,560
operations you need to perform later. The
way tpm2-tools work is not just creating

141
00:12:53,560 --> 00:13:00,360
the key and having it in the TPM but
also providing us a way to save the

142
00:13:00,360 --> 00:13:06,480
context of the TPM at that moment. The
context as the TPM is loaded with its

143
00:13:06,480 --> 00:13:11,760
properties. Using the -c argument we
can save that context, which makes it so

144
00:13:11,760 --> 00:13:17,399
much easier to use later for child key
generation, and other operations.

145
00:13:17,399 --> 00:13:22,680
This would be the recommended path
forward. As mentioned earlier the user of

146
00:13:22,680 --> 00:13:28,320
the TPM has the ability to provide
additional entropy to the KDF process.

147
00:13:28,320 --> 00:13:34,680
This is the using the -u option. And
it can be provided as a file or from the

148
00:13:34,680 --> 00:13:41,279
standard input. Here we have two simple
examples of generating primary keys.

149
00:13:41,279 --> 00:13:48,760
The one generates an ECC P-256 curve key the
other an RSA 2048-bit key. In the one case

150
00:13:48,760 --> 00:13:53,519
we have not provided a hierarchy we have
not defined a specific hierarchy. Because

151
00:13:53,519 --> 00:13:57,839
the default is the owner's hierarchy. It
is perhaps better to have that

152
00:13:57,839 --> 00:14:02,040
explicitly when running in commands just
to make sure you know where the key is

153
00:14:02,040 --> 00:14:10,519
created on. Notice that the parent object
is provided with capital C dash C (-C)

154
00:14:10,519 --> 00:14:16,480
And the object being generated or the
context in this case is with dash lower-case c (-c).

155
00:14:16,480 --> 00:14:22,839
This kind of naming is preserved
across the tools. You will see this type

156
00:14:22,839 --> 00:14:28,880
of naming across all tpm2-tools. Often
the key that we generate would have have

157
00:14:28,880 --> 00:14:34,040
to interact with other systems not just
the TPM. And more likely it will be the

158
00:14:34,040 --> 00:14:38,320
public portion of the key. For that
purpose the tpm2-tools provide a very

159
00:14:38,320 --> 00:14:46,000
easy interface to output the TPM public
key in a well-known format. By default

160
00:14:46,000 --> 00:14:50,360
this is the TSS format that will just
give us the binary blob with a private

161
00:14:50,360 --> 00:14:54,560
protected part and the public part
according to the TPM specification.

162
00:14:54,560 --> 00:15:00,600
What we usually need is
either a PEM or a DER format for various

163
00:15:00,600 --> 00:15:06,199
tools. Not just OpenSSL but open... it
comes in an OpenSSL-compatible format.

164
00:15:06,199 --> 00:15:09,680
This is important to note if you're
using a different cryptographic library.

165
00:15:09,680 --> 00:15:15,279
-o specifies the file name of the
output, -f specifies only the format.

166
00:15:15,279 --> 00:15:19,399
Now that we have a primary key we can
generate a child key. For that purpose we

167
00:15:19,399 --> 00:15:24,880
have another easily to guess tpm2-tool
named tpm2_create. This tool takes the

168
00:15:24,880 --> 00:15:30,839
parent context, which is no longer the
hierarchy, but the primary context of the

169
00:15:30,839 --> 00:15:34,920
context of the primary key. And it comes
with a capital -C. Then we have the child

170
00:15:34,920 --> 00:15:39,480
context. Which will be the output, the
result of our operation of the TPM key.

171
00:15:39,480 --> 00:15:42,959
This will store the whole key the
private and the public part. But the

172
00:15:42,959 --> 00:15:47,720
private part is already encrypted and
protected before leaving the TPM. So even

173
00:15:47,720 --> 00:15:53,839
though this is stored outside the TPM,
this is secure. Other than the context of

174
00:15:53,839 --> 00:15:59,839
the TPM, we can also store the separate
parts of the key as private and public.

175
00:15:59,839 --> 00:16:04,720
In some situations this is easier. In
other situations this will be needed.

176
00:16:04,720 --> 00:16:09,759
It really depends on the operations you
plan to use from the TPM.

177
00:16:09,759 --> 00:16:16,279
For the purposes of our exercises we'll use the
TPM context option. So we'll be saving

178
00:16:16,279 --> 00:16:21,040
mostly the TPM context and not the
separate private and public part. Here is

179
00:16:21,040 --> 00:16:26,880
an example of generating a child ECC 256
key. We have done more than just

180
00:16:26,880 --> 00:16:31,639
specifying the type of key, but we have also
specified the signing scheme we want, to

181
00:16:31,639 --> 00:16:38,920
ECDSA. Also when we created the key in the
first example we save the key as private

182
00:16:38,920 --> 00:16:44,319
and public part, rather as a TPM context, 
that we can later use. In the first

183
00:16:44,319 --> 00:16:48,839
example we would need to execute tpm2_load with the private and the public

184
00:16:48,839 --> 00:16:53,319
part, to start using the keys. It is
important to know that the primary key

185
00:16:53,319 --> 00:16:59,399
type has no impact on the child key type.
Remember that the primary keys can only

186
00:16:59,399 --> 00:17:05,039
be asymmetric. But the child keys can be
asymmetric, symmetric, or keyed hash.

187
00:17:05,039 --> 00:17:09,240
The is the reason for that design choice. The
primary key is used just to protect the

188
00:17:09,240 --> 00:17:14,199
child keys and provide seed. Therefore
you can even have a RSA primary key

189
00:17:14,199 --> 00:17:20,199
with an ECC child key. I would recommend
sticking to the same type of key if you

190
00:17:20,199 --> 00:17:26,120
have an ECC child key keep to an EC
primary key. And also keep in mind RSA

191
00:17:26,120 --> 00:17:31,200
operations in general take more time. But
this is up to you to decide based on

192
00:17:31,200 --> 00:17:36,240
your system and application requirements.
While child keys can be stored in several

193
00:17:36,240 --> 00:17:41,799
forms, if you decide to use the
tpm2-tools more, I think you would appreciate

194
00:17:41,799 --> 00:17:46,840
having the TPM context more than having
the separate private and public part.

195
00:17:46,840 --> 00:17:51,000
At the same time, to integrate with
different systems make sure to use the

196
00:17:51,000 --> 00:17:56,600
formatting option, to get a well-known
format, that can be integrated easily.

197
00:17:56,600 --> 00:18:02,520
If you generate the TPM key a separate part
as mentioned please make sure you use

198
00:18:02,520 --> 00:18:08,600
the tpm2_load command. Otherwise the
TPM would not have actually have your

199
00:18:08,600 --> 00:18:13,799
key ready for use. And in most TPM there
is a command that can do these operations

200
00:18:13,799 --> 00:18:19,679
together. For the purposes of this course
we'll always do that separately ,or use

201
00:18:19,679 --> 00:18:25,080
the TPM context. This is the typical
workflow for our labs, where we will

202
00:18:25,080 --> 00:18:30,159
generate a primary key, child key, and
perform the operation necessary for

203
00:18:30,159 --> 00:18:34,720
achieving the goal of the exercise. Then
we will flush the TPM objects because

204
00:18:34,720 --> 00:18:41,400
the TPM has limited storage. It has
usually three to seven key slots for

205
00:18:41,400 --> 00:18:46,320
keys in memory ready for use. And the
same amount of number three to seven

206
00:18:46,320 --> 00:18:50,400
depending on the model for persistent
keys, that are constantly available, that

207
00:18:50,400 --> 00:18:55,039
we can swap out place again. We'll talk
about this in another lecture.

208
00:18:55,039 --> 00:18:59,640
Just remember that if you run out of key
slots you can always use the tpm2_flushcontext

209
00:18:59,640 --> 00:19:04,080
command. And depending on what
you want to flush if it's a newly

210
00:19:04,080 --> 00:19:09,679
created object then it's probably a
transient object. If it's a TPM session

211
00:19:09,679 --> 00:19:15,600
where, we're not there yet, but there is
the -l switch to flush loaded sessions

212
00:19:15,600 --> 00:19:21,720
and save sessions. To seal data inside a
newly generated key we have to use the

213
00:19:21,720 --> 00:19:28,320
-i switch to the tpm2_create command.
Once generate our child key we can

214
00:19:28,320 --> 00:19:33,559
perform various TPM operations. The
operation here is special with that it

215
00:19:33,559 --> 00:19:39,120
happens and takes place during key
generation. The TPM can seal in two

216
00:19:39,120 --> 00:19:45,120
different ways. The one is inside a
freshly generated TPM key. The other is

217
00:19:45,120 --> 00:19:49,880
against platform configuration registers
called PCRs. And we will learn more about

218
00:19:49,880 --> 00:19:56,480
PCRs in the advanced TPM course. Sealing
the secret inside the key is limited by

219
00:19:56,480 --> 00:20:03,400
size because it takes part of the TPM
key structure. The size is 128 bytes.

220
00:20:03,400 --> 00:20:08,960
This is plenty for sealing a secret
configuration string, or even a symmetric

221
00:20:08,960 --> 00:20:14,840
key. An example of how to seal some
arbitrary data is provided on this slide.

222
00:20:14,840 --> 00:20:19,559
And please note that the sealing happens
during key generation. What's missing in

223
00:20:19,559 --> 00:20:24,440
this example is the generation of the
primary key that happened some time

224
00:20:24,440 --> 00:20:30,720
before. When we have our object our
sealed object we can unseal it using the

225
00:20:30,720 --> 00:20:37,080
tpm2_unseal comand. If we have set a
password phrase to protect our child key

226
00:20:37,080 --> 00:20:42,640
then we would have to provide it to the
tpm2_unseal. The output of the tpm2_unseal

227
00:20:42,640 --> 00:20:46,200
command is usually the common
line, the terminal window that we have

228
00:20:46,200 --> 00:20:51,280
open. We can redirect that with the -o
option. Creating digital signatures using

229
00:20:51,280 --> 00:20:55,400
the TPM requires the child key to
already exist. In contrast with the

230
00:20:55,400 --> 00:21:00,159
previous operation where we sealed data
inside the TPM key, and we had to do

231
00:21:00,159 --> 00:21:05,799
it during generation, here we need to
have the key generated and loaded into

232
00:21:05,799 --> 00:21:10,159
the TPM to be ready for use. We can
generate digital signatures using the

233
00:21:10,159 --> 00:21:16,679
tpm2_sign tool. There is a mandatory
option -o t store the signature which is

234
00:21:16,679 --> 00:21:22,799
the result of the operation. The opposite
procedure verification has the tpm2_verifysignature

235
00:21:22,799 --> 00:21:28,240
tool. There we would
have to provide the original message and

236
00:21:28,240 --> 00:21:33,640
the signature we generated with tpm2_sign.
There's an example below. For any

237
00:21:33,640 --> 00:21:39,520
questions please use the discussion
boards after each section this helps us

238
00:21:39,520 --> 00:21:42,720
understand more context about the
troubles you might experience or the

239
00:21:42,720 --> 00:21:46,640
difficulties you might have if you need
an email contact feel free to reach out

240
00:21:46,640 --> 00:21:51,919
at our course
email.